﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task.Models;

namespace Task.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        public ActionResult LoginUser(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                this.RedirectToAction("Index", "Home");
            }

            return View();
        }
    }
}