import { ModuleWithProviders } from "@angular/core";
import { Router, RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { AboutComponent } from "./components/component.about"
import { ContactComponent } from "./components/component.contact"
import { HomeComponent } from "./components/component.home"
import { LoginComponent } from "./components/component.login"
import { EmptyComponent } from "./components/component.empty"
import { NavigationComponent } from "./components/component.navigation"

export const router: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'empty', component: EmptyComponent },
    { path: 'navigation', component: NavigationComponent }
    //{ path: '**', component: HomeComponent } // This is  temporary solution we will have to resolve this issue in future.

];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);