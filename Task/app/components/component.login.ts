﻿/// <reference path="../models/login.ts" />
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { Login } from '../models/login';
import { LoginService } from '../services/login.service';


@Component({
    selector: 'login',
    templateUrl: 'app/components/login.html',
    providers: [LoginService]

})
export class LoginComponent {

    login: Login = new Login();

    constructor(private _loginService: LoginService, private _router: Router, private _module: FormsModule) {
        debugger;
        console.log('Login Component Loaded');
        this.login.username = "tushar";
        this.login.password = "tag";
    }

    onLoginClick() {
        debugger;
        console.log('Home redirect');
        this._router.navigate(['/navigation']);
    }
}
