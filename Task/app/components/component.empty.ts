﻿import { Component, OnInit } from "@angular/core";
import { Login } from '../models/login';
import { Router, RouterModule } from "@angular/router";


@Component({
    selector: 'empty',
    templateUrl: 'app/components/empty.html'
})

export class EmptyComponent{

    login: Login = new Login();
    constructor(private _router: Router) {
        this.login.isAuthenticate = false;
        console.log('Empty Component Loaded');
    }

}