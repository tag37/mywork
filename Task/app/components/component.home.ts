﻿import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'home',
    templateUrl: 'app/components/home.html'
})

export class HomeComponent {
    constructor(private _router: Router) {
    }

    ngOnInit(): void {        
        console.log('Enter into login navigation');
    }
}