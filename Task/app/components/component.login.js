"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../models/login.ts" />
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var login_1 = require("../models/login");
var login_service_1 = require("../services/login.service");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(_loginService, _router, _module) {
        this._loginService = _loginService;
        this._router = _router;
        this._module = _module;
        this.login = new login_1.Login();
        debugger;
        console.log('Login Component Loaded');
        this.login.username = "tushar";
        this.login.password = "tag";
    }
    LoginComponent.prototype.onLoginClick = function () {
        debugger;
        console.log('Home redirect');
        this._router.navigate(['/navigation']);
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'app/components/login.html',
            providers: [login_service_1.LoginService]
        }),
        __metadata("design:paramtypes", [login_service_1.LoginService, router_1.Router, forms_1.FormsModule])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=component.login.js.map