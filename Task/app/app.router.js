"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var component_about_1 = require("./components/component.about");
var component_contact_1 = require("./components/component.contact");
var component_home_1 = require("./components/component.home");
var component_login_1 = require("./components/component.login");
var component_empty_1 = require("./components/component.empty");
var component_navigation_1 = require("./components/component.navigation");
exports.router = [
    { path: 'about', component: component_about_1.AboutComponent },
    { path: 'contact', component: component_contact_1.ContactComponent },
    { path: 'home', component: component_home_1.HomeComponent },
    { path: 'login', component: component_login_1.LoginComponent },
    { path: 'empty', component: component_empty_1.EmptyComponent },
    { path: 'navigation', component: component_navigation_1.NavigationComponent }
    //{ path: '**', component: HomeComponent } // This is  temporary solution we will have to resolve this issue in future.
];
exports.routes = router_1.RouterModule.forRoot(exports.router);
//# sourceMappingURL=app.router.js.map