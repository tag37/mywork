import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routes } from './app.router';
import { AboutComponent } from './components/component.about';
import { ContactComponent } from './components/component.contact';
import { HomeComponent } from './components/component.home';
import { LoginComponent } from "./components/component.login";
import { EmptyComponent } from "./components/component.empty";
import { NavigationComponent } from "./components/component.navigation";


@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, routes],
    declarations: [AppComponent, AboutComponent, ContactComponent, HomeComponent, LoginComponent, EmptyComponent, NavigationComponent],
    bootstrap: [AppComponent]
})

export class AppModule { }
