import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from './services/login.service';
@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    providers: [LoginService]
})
export class AppComponent {
    name = 'Angular 4';
    constructor(private _router: Router) {
    }

    ngAfterContentInit(): void {
        console.log('Enter into login navigation');
    }
}
