﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Task
{
    public class RouteConfig
    {
        public class ServerRouteConstraint : IRouteConstraint
        {
            private readonly Func<Uri, bool> _predicate;

            public ServerRouteConstraint(Func<Uri, bool> predicate)
            {
                this._predicate = predicate;
            }

            public bool Match(HttpContextBase httpContext, Route route, string parameterName,
                RouteValueDictionary values, RouteDirection routeDirection)
            {
                return this._predicate(httpContext.Request.Url);
            }
        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            //    constraints: new
            //    {
            //        serverRoute = new ServerRouteConstraint(url =>
            //        {
            //            return url.PathAndQuery.StartsWith("/Settings",
            //                StringComparison.InvariantCultureIgnoreCase);
            //        })
            //    });

            routes.MapRoute(
                name: "angular",
                url: "{*url}",
                defaults: new { controller = "Account", action = "Login" } // The view that bootstraps Angular 2
            );
        }
    }
}
